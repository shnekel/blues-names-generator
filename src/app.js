'use strict';

const NICKNAMES = 'Fat Buddy Sticky Old Texas Hollerin\' Ugly Brown Happy Boney Curly Pretty Jailhouse Peg-Leg Red Sleepy Bald Skinny Blind Big Yella Toothless Screamin\' Fat-Boy Washboard Steel-Eye Crippled Lightnin\''.split(' ');
const FIRSTNAMES = 'Bones Money Harp Legs Eyes Lemon Killer Hips Lips Fingers Boy Liver Gumbo Foot Mama Back Duke Dog Bad-Boy Baby Chicken Pickles Sugar Willy Tooth Smoke'.split(' ');
const LASTNAMES = "Jackson McGee Hopkins Dupree Green Brown Jones Rivers Malone Washington Smith Parker Lee Thompkins King Bradley Hawkins Jefferson Davis Franklin White Jenkins Bailey Johnson Blue Alison Obama".split(' ');

class App {
    constructor(parent) {
        this.parent = document.getElementById(parent);
        this.generate = document.getElementById('generate');
        this.generate.addEventListener('click', this.run.bind(this));
    }

    pickFrom(list) {
        return list[Math.floor(Math.random() * list.length)].replace('-', ' ') + " ";
    }
    
    generateBluesName() {
        return this.pickFrom(NICKNAMES) + this.pickFrom(FIRSTNAMES) + this.pickFrom(LASTNAMES);
    }

    run() {
        this.parent.innerHTML = this.generateBluesName();
    }
}

(new App('result')).run();



